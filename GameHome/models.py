from django.db import models

# Create your models here.
class Account(models.Model):
    accountid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50, blank=True, null=True)
    password = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(null=True)
    createdate  = models.DateField(auto_now_add=True, blank=True, null=True)
    editdate = models.DateTimeField(auto_now=True, blank=True, null=True)
    isenable = models.BooleanField(default=True, blank=True, null=True)

    def __str__(self):
        return self.username
    
    class Meta:
        managed = True
        db_table = 'Account'

class Game(models.Model):
    gameid = models.AutoField(primary_key=True)
    gameorder = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    createdate  = models.DateField(auto_now_add=True, blank=True, null=True)
    editdate = models.DateTimeField(auto_now=True, blank=True, null=True)
    isenable = models.BooleanField(default=True, blank=True, null=True)
    note = models.TextField(max_length=250, blank=True, null=True)

    tracking = models.ManyToManyField(Account)

    def __str__(self):
        return self.title
    
    class Meta:
        managed = True
        db_table = 'Game'