from django.urls import path
from GameHome import views

app_name = 'GameHome'

urlpatterns = [
    path('', views.index, name='index'),
    path('dang-nhap/', views.login, name='login'),
    path('dang-ky/', views.register, name='register'),
    path('gioi-thieu/', views.review, name='review'),
    path('tro-choi/', views.games, name='games'),
    path('tin-tuc/', views.blog, name='blog'),
    path('lien-he/', views.contact, name='contact'),
    path('quen-mat-khau/', views.forgetpassword, name='forgetpassword')
]