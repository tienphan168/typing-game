from django.shortcuts import render, redirect
from typinggame.settings import EMAIL_HOST_USER
from django.core.mail import send_mail
from .models import *
import random

# Create your views here.
def index(request):
    return render(request, 'homepage/index.html')

def review(request):
    return render(request, 'homepage/review.html')

def games(request):
    return render(request, 'homepage/games.html')

def blog(request):
    return render(request, 'homepage/blog.html')

def contact(request):
    return render(request, 'homepage/contact.html')

def login(request):
    return render(request, 'homepage/login.html')

def register(request):
    message = ''
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        if password == confirm_password:
            if Account.objects.filter(username=username).exists():
                message = "Tên tài khoản đã được sử dụng"
                return render(request, 'homepage/register.html', {'message':message})
            else:
                if Account.objects.filter(email=email).exists():
                    message = "Email đã được sử dụng"
                    return render(request, 'homepage/register.html', {'message':message})
                else:
                    user = Account(username=username, email=email, password=password)
                    user.save()
                    message = "Đăng ký thành công, hãy đăng nhập"
                    return render(request, 'homepage/register.html', {'message':message})
        else:
            message = "Mật khẩu không trùng khớp"
            return render(request, 'homepage/register.html', {'message':message})
    return render(request, 'homepage/register.html')

def login(request):
    message = ''
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = Account.objects.filter(username=username,password=password)

        if user is not None:
            context = {
                'user':user,
                'username':username,
            }
            return render(request, 'homepage/index.html', context)
        else:
            message = "Tài khoản hoặc mật khẩu không đúng"
            context = {
                'message':message,
                'user':user,
            }
            return render(request,'homepage/login.html', context) 
    else:
       return render(request,'homepage/login.html')

def forgetpassword(request):
    if request.method == 'POST':
        email = request.POST['email']
        user_mail = Account.objects.filter(email=email)
        characters = list('abcdefghijklmnopqrstuvwxyz')
        new_password = ''
        if user_mail.exists():
            for i in range(6):
                new_password += random.choice(characters)
            user_mail = Account(password=new_password)
            user_mail.save()
            subject = 'Typing-Game: Lấy lại mật khẩu'
            message = 'Mật khẩu mới của bạn là: '
            send_mail(subject, message, EMAIL_HOST_USER, [email], fail_silently = False)
            return render(request, 'homepage/login.html')
        else:
            message = "Email không tồn tại, hãy đăng ký tài khoản"
            return render(request, 'homepage/forget_password.html', {'message':message})
    return render(request, 'homepage/forget_password.html')